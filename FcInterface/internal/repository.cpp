#include "repository.h"

namespace Repository
{
	FeatureRepository& Get()
	{
		// Allocate this on the heap so it never destructs as we'll be accessing it during global destruction.
		static FeatureRepository* gInterfaceRepository = new FeatureRepository;
		return *gInterfaceRepository;
	}
}

void FeatureRepository::AppendFeature( const CFeature& feature )
{
	featureBenches.push_back( feature );
}

CFeature* FeatureRepository::FindFeature( const FeatureIdentity& feature )
{
	if( feature.priority == FeatureIdentity::TOP_PRIORITY )
	{
		auto features = FindFeature( feature.featureName );
		if( !features.empty() )
		{
			return features.front();
		}

		return nullptr;
	}
	else
	{
		return featureBenches.find( [=](const CFeature& Feature)->bool
		{
			return std::strcmp( Feature.id.featureName.c_str(), feature.featureName ) == 0 && Feature.id.priority == feature.priority;
		});
	}
}

std::vector<CFeature*> FeatureRepository::FindFeature( const char* featureName )
{
	std::vector<CFeature*> features;

	featureBenches.find( [&](CFeature& feature)->bool
	{
		if( std::strcmp( feature.id.featureName.c_str(), featureName ) == 0 )
		{
			features.push_back( &feature );
		}

		return false;
	});

	std::sort( features.begin(), features.end(), [](const CFeature* f1, const CFeature* f2 )
	{
		return f1->id.priority > f2->id.priority;
	});

	return features;
}