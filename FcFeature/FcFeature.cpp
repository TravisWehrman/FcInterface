#include "FcFeature.h"

// Implements "FeatureInADLL"; this would usually in a separate .cpp
class CConcreteFeature final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		return "FeatureInADLL_VS";
	}

	int GetInt() override
	{
		return 42;
	}

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER( CConcreteFeature, IFLOWCALFeatureInADLL_VS, FeatureInADLL_VS ); // TODO: ensure these are synced?

class CConcreteFeature2 final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		//return "Called '" __FUNCTION__ "()' from '" "Feature.dll'\n";
		return "FeatureInADLL2_VS";
	}

	int GetInt() override
	{
		return 24;
	}

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER( CConcreteFeature2, IFLOWCALFeatureInADLL_VS, FeatureInADLL2_VS );