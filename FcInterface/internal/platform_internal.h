#pragma once

using ExportSig = std::pair<std::string, int>;

#if defined(WIN32)
#include <Windows.h>
#include <imagehlp.h>
#pragma comment(lib, "Imagehlp.lib")


EL_SHARED_LINKAGE const std::string module_feature_prefix = FCI_TO_STRING(EL_MODULE_FEATURE_PREFIX);

inline void FcAddDllDirectory( const std::string& path )
{
	std::wstring wPath;
	wPath.assign( path.begin(), path.end() );
	AddDllDirectory( wPath.c_str() );
}

inline fs::path GetEXEDirectory()
{
	char buf[MAX_PATH];
	HMODULE mod = nullptr;

	GetModuleHandleExA( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCSTR)&GetEXEDirectory, &mod );

	GetModuleFileNameA( mod, buf, sizeof( buf ) );

	const fs::path exePath = fs::canonical( fs::path(buf).make_preferred() );
	const fs::path exeFolderPath = exePath.parent_path() += fs::path::preferred_separator;
	return exeFolderPath;
}

inline FeatureIdentityInternal ExtractFeatureIdenityFromExportName(std::string exportName )
{
	// We're going to ignore any characters before the prefix as some compilers (BCC) still inject export prefixes with extern "C"
	const auto prefix_end = exportName.find( module_feature_prefix ) + module_feature_prefix.length();
	const auto priority_end = exportName.find( "_", prefix_end );
	try
	{
		const std::string priority = exportName.substr( prefix_end, priority_end - prefix_end );
		const bool isStringOnlyNumbers = std::all_of(priority.begin(), priority.end(), ::isdigit);

		if( !isStringOnlyNumbers || priority.empty() )
		{
			return {};
		}

		const int Priority = atoi( priority.c_str() );

		// This could throw if there is no suffix; which shouldn't happen but is an external case.
		const std::string featureName = exportName.substr( priority_end + 1 );

		return {featureName, "", Priority};
	}
	catch(...){} // Just eat it and skip this invalid special case

	return {};
}

// LoadLibraryEx::DONT_RESOLVE_DLL_REFERENCES is deprecated. Let's just use Window's ImageHelper for this
inline std::vector<ExportSig> GatherFeaturesFromModule( const char* dllName )
{
	_LOADED_IMAGE LoadedImage;
	std::vector<ExportSig> exportNames;
	if( MapAndLoad( dllName, nullptr, &LoadedImage, TRUE, TRUE ) )
	{
		unsigned long cDirSize;
		const _IMAGE_EXPORT_DIRECTORY* ImageExportDirectory = static_cast<_IMAGE_EXPORT_DIRECTORY*>( ImageDirectoryEntryToData( LoadedImage.MappedAddress, false, IMAGE_DIRECTORY_ENTRY_EXPORT, &cDirSize ) );
		if( ImageExportDirectory )
		{
			const DWORD* dNameRVAs = static_cast<DWORD*>( ImageRvaToVa( LoadedImage.FileHeader, LoadedImage.MappedAddress, ImageExportDirectory->AddressOfNames, nullptr ) );
			for( size_t i = 0; i < ImageExportDirectory->NumberOfNames; ++i )
			{
				const std::string exportName = static_cast<const char*>( ImageRvaToVa( LoadedImage.FileHeader, LoadedImage.MappedAddress, dNameRVAs[i], nullptr ) );

				if( exportName.find( module_feature_prefix ) != std::string::npos )
				{
					const auto feature = ExtractFeatureIdenityFromExportName( exportName );
					
					if( !feature.featureName.empty() )
					{
						exportNames.push_back( {feature.featureName, feature.priority} );
					}
				}
			}
		}
		UnMapAndLoad( &LoadedImage );
	}

	return exportNames;
}
#endif // WIN32