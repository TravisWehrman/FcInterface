#pragma once

#include <string>

#define FCI_CPP17_SUPPORT ((__cplusplus >= 201703L))

#if FCI_CPP17_SUPPORT // For global variables
#define EL_SHARED_LINKAGE inline // one instance of each FP per module
#else
#define EL_SHARED_LINKAGE static // one instance of each FP per translation unit
#endif

#if defined(_WIN32) // Only implementing Windows atm

#define EL_EXPORT extern "C" __declspec(dllexport)

bool FcIsModuleLoaded( const char* moduleName );
void* FcGetProcAddress( const char* moduleName, const char* procName );
void FcLoadLibraryIfNotLoaded( const char* moduleName );
std::string FcGetThisModuleName( const char* (*funcInModule)() );

#if (_MSC_VER >= 1800 /*VS2013*/)
#define FCI_IS_DLL _USRDLL
#define FCI_IS_STATIC_LIB _LIB
#define FCI_EXPORT_CALLCONV __cdecl // Visual Studio prefixes exports with _ via __stdcall while Embarcadero does it with __cdecl
#elif (__BORLANDC__ >= 0x0710 /*BCC32C Support*/)
#define FCI_IS_DLL __DLL__
#define FCI_IS_STATIC_LIB 0 // Not defined in Embarcadero
#define FCI_EXPORT_CALLCONV __cdecl
#else
#error "Unsupported Platform"
#endif

// VC++ and BCC32 use different default calling conventions, __cdecl and __stdcall respectively
// so we need to share the same so we can have FcInterface support across compilers.
#define FCI_CALLCONV __cdecl
#else
#error "FcInterface TODO: Map other platform OS calls"
#endif

#include "filesystem.h"

#if !defined(EL_MINIMAL_INTERFACE) // keep OS headers out of this header
#include "platform.cpp"
#endif

#define FCI_IS_EXE !(FCI_IS_STATIC_LIB || FCI_IS_DLL) // Is .exe project
// NOTE: You must manually add the appropriate define if the project was originally created with a different type in VS.

#define fci_str(a) #a
#define FCI_TO_STRING(a) fci_str(a)