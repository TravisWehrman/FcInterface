#include "catch.hpp"

#include "FcFeature/FcFeature.h"

#include <chrono>
#include <iomanip>
#include <sstream>
#include <unordered_set>

// Declare a local Feature
struct ITestFcInterface
{
	virtual int Get42() = 0;
	virtual int Get43() = 0;
	virtual void ThrowException() = 0;
	virtual void DoNothing() = 0;

	void NonVirt(){};
};
FCINTERFACE_DECLARE_EXISTING( ITestFcInterface, MyLocalFeature )

struct CTestFcInterface final : public ITestFcInterface
{
	int Get42() override { return 42; }
	void DoNothing() override {}
	int Get43() override { return 43; }

	int MyFunNonVirt() { return 44; }

	void ThrowException() override { throw "Feature Pointer threw this exception"; }
};
FCINTERFACE_REGISTER( CTestFcInterface, ITestFcInterface, MyLocalFeature )

// Custom Catch Matchers
struct HandleNullptrStringEquals : public Catch::MatcherBase<const char*>
{
	enum class NullCompare
	{
		TREAT_AS_DIFFERENT,
		TREAT_AS_EQUAL,
	};

	explicit HandleNullptrStringEquals( const char* c_str, Catch::CaseSensitive::Choice caseCompare, NullCompare nullcmp ) :
		c_str_( c_str ), caseCompare_( caseCompare ), nullcompare( nullcmp ){}

	bool match( const char* const& cmp_str ) const override
	{
		if( nullcompare == NullCompare::TREAT_AS_EQUAL && !c_str_ && !cmp_str )
			return true;

		if( !c_str_ || !cmp_str )
			return false;

		if( caseCompare_ == Catch::CaseSensitive::No )
		{
			auto icompare = []( std::string lhs, std::string rhs )->bool
			{
				if( lhs.length() == rhs.length() )
				{
					std::transform( lhs.begin(), lhs.end(), lhs.begin(), ::tolower );
					std::transform( rhs.begin(), rhs.end(), rhs.begin(), ::tolower );
					return lhs == rhs;
				}

				return false;
			};

			return icompare( c_str_, cmp_str ) ;
		}

		return std::strcmp( c_str_, cmp_str ) == 0;

	}

	virtual std::string describe() const override 
	{
		if( !c_str_ )
		{
			return "does not match {null string} (two null char* are treated as nonmatching)";
		}
		else
		{
			std::ostringstream ss;
			ss << "does not match \"" << c_str_ << "\"";
			return ss.str();
		}
	}

private:

	const char* c_str_;
	Catch::CaseSensitive::Choice caseCompare_;
	NullCompare nullcompare;

};
HandleNullptrStringEquals Equals(	const char* c_str,
							HandleNullptrStringEquals::NullCompare nullCompare = HandleNullptrStringEquals::NullCompare::TREAT_AS_DIFFERENT,
							Catch::CaseSensitive::Choice caseSensitive = Catch::CaseSensitive::Yes )
{
	return HandleNullptrStringEquals( c_str, caseSensitive, nullCompare );
}
using Catch::Matchers::Equals;


// Domain-related Helpers
FcInterfaceInternals::FeatureQuery QueryFeature( const FcInterfaceInternals::FeatureIdentity& feature )
{
	return FcInterfaceInternals::GetRepoInstance()->QueryFeature( feature );
}


struct ExpectedFeatureState final
{
	const char* name;
	const char* moduleName;
	FcInterfaceInternals::eFeatureQueryStatus status;
};

// Since Feature Pointers have global state, we need to reset it between tests
void ResetAllLocalFeaturePointers()
{
	FcInterfaceInternals::FeaturePointerUtils::Reset( MyLocalFeature );
	FcInterfaceInternals::FeaturePointerUtils::Reset( FeatureInADLL_VS );
	FcInterfaceInternals::FeaturePointerUtils::Reset( FeatureInADLL2_VS );
	FcInterfaceInternals::FeaturePointerUtils::Reset( FeatureInADLL_NotRegistered_VS );
	FcInterfaceInternals::FeaturePointerUtils::Reset( FeatureInAnotherDLL_VS );
}

void RegisterAllLocalFeaturePointers()
{
	FcInterfaceInternals::FeaturePointerUtils::Register( MyLocalFeature );
	FcInterfaceInternals::FeaturePointerUtils::Register( FeatureInADLL_VS );
	FcInterfaceInternals::FeaturePointerUtils::Register( FeatureInADLL2_VS );
	FcInterfaceInternals::FeaturePointerUtils::Register( FeatureInADLL_NotRegistered_VS );
	FcInterfaceInternals::FeaturePointerUtils::Register( FeatureInAnotherDLL_VS );
}

void ResetLocalFeatures()
{
	Fc_g_CreateCTestFcInterfaceITestFcInterface_reg.Register();
}

template<typename TFeaturePointer>
void ValidateInitialStateOfFeaturePointer( const TFeaturePointer& feature, const ExpectedFeatureState expectedState )
{
	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( feature ) );
	REQUIRE_THAT( FcInterfaceInternals::FeaturePointerUtils::GetFeatureName( feature ), Equals( expectedState.name ) );
}

void ValidateInitialStateOfFeature( const ExpectedFeatureState expectedState, bool allowUnavailableState = false )
{
	const auto query = QueryFeature( {expectedState.name, expectedState.moduleName, 0} );

	if( allowUnavailableState && query.status == FcInterfaceInternals::eFeatureQueryStatus::UNKNOWN_FEATURE )
	{
		return;
	}
	
	REQUIRE( query.status == expectedState.status );
	REQUIRE_THAT( query.id.featureName, Equals( expectedState.name ) );
	REQUIRE_THAT( query.id.moduleName, Equals( expectedState.moduleName, HandleNullptrStringEquals::NullCompare::TREAT_AS_EQUAL, Catch::CaseSensitive::No ) );

	REQUIRE( (query.fp != nullptr) == (query.status == FcInterfaceInternals::eFeatureQueryStatus::FEATURE_READY) );
}

void ValidateInitialState_MyLocalFeature()
{
	const ExpectedFeatureState expected = {	"MyLocalFeature",
											"FcInterfaceTests.exe",
											FcInterfaceInternals::eFeatureQueryStatus::FEATURE_READY };

	ValidateInitialStateOfFeature( expected, true );
	ValidateInitialStateOfFeaturePointer( MyLocalFeature, expected );
}

void ValidateInitialState_FeatureDll()
{
	const ExpectedFeatureState expected = {	"FeatureInAnotherDLL_VS",
											"Feature2.dll",
											FcInterfaceInternals::eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE };

	ValidateInitialStateOfFeature( expected, true );
	ValidateInitialStateOfFeaturePointer( FeatureInAnotherDLL_VS, expected );
}

void ValidateInitialState_Feature2Dll()
{
	const ExpectedFeatureState expected1 = { "FeatureInADLL_VS",
											"Feature.dll",
											FcInterfaceInternals::eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE };

	const ExpectedFeatureState expected2 = { "FeatureInADLL2_VS",
											"Feature.dll",
											FcInterfaceInternals::eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE };

	const ExpectedFeatureState expected3 = { "FeatureInADLL_NotRegistered_VS",
											nullptr,
											FcInterfaceInternals::eFeatureQueryStatus::UNKNOWN_FEATURE };

	ValidateInitialStateOfFeaturePointer( FeatureInADLL_VS, expected1 );
	ValidateInitialStateOfFeaturePointer( FeatureInADLL2_VS, expected2 );
	ValidateInitialStateOfFeaturePointer( FeatureInADLL_NotRegistered_VS, expected3 );

	ValidateInitialStateOfFeature( expected1, true );
	ValidateInitialStateOfFeature( expected2, true );
	ValidateInitialStateOfFeature( expected3 );
}

void ValidateInitialForAllFeaturePointers()
{
	ValidateInitialState_MyLocalFeature();
	ValidateInitialState_FeatureDll();
	ValidateInitialState_Feature2Dll();
}

void Validate_ModuleIsFree( const char* moduleName )
{
	HMODULE mod = GetModuleHandleA( moduleName );

	if( mod )
	{
		FreeLibrary( mod );
	}
	else
	{
		return;
	}

	REQUIRE( !GetModuleHandleA( moduleName ) );
}

void GlobalStateReset()
{
	ResetAllLocalFeaturePointers();

	Validate_ModuleIsFree( "Feature" );
	Validate_ModuleIsFree( "Feature2" );
	Validate_ModuleIsFree( "PriorityOverride" );
	Validate_ModuleIsFree( "DuplicateFeature" );

	INFO("This test requires that the repoistory lives in its own DLL (e.g. not in this exe)")
	REQUIRE( EL_REPO_MODULE_NAME != nullptr );
	Validate_ModuleIsFree( EL_REPO_MODULE_NAME );

	FcInterfaceInternals::GetModuleState().Reset();

	ValidateInitialForAllFeaturePointers();
	ResetLocalFeatures();

	// Reregister since the repo DLL was unloaded
	RegisterAllLocalFeaturePointers();
}

bool IsRepoAvaliable()
{
	return FcInterfaceInternals::GetRepoFetcherFn();
}

TEST_CASE(	"Invoking a Feature Pointer whose Feature's DLL is linked at loadtime by the OS should register cached features from said DLL"
			" when the repository is in a separate DLL, thus resulting in a proper invocation."
			" This issue may be caused when a Feature registers with the mock repo when the real repo is unavailable, but never gets properly registered.",
	"[Infrastructure][FcInterface][HasExternalDependency][!hide]")
{
	// Ensure we don't have the repository avaliable
	// Doing this instead of linking to the DLL for convenience.
	REQUIRE( !IsRepoAvaliable() );
	REQUIRE( LoadLibraryA( "Feature" ) );

	// NOTE: This fails in Embarcadero-to-Visual Studio.
	// GetProcAddress fails to fetch VS DLL features to invoke for registration (returns nullptr)
	// I'm not sure why this is the case, but I'm letting it slide for now since this will not be a required usecase for my current needs
	// This issue only seems to occurs when we load a Feature DLL before the Repo DLL is initilized. (Comenting out LoadLibrary() above does not see this issue)
	REQUIRE_NOTHROW( FeatureInADLL_VS->DoNothing() );

	GlobalStateReset();
}

TEST_CASE( "Initialize EasyLink by invoking the first feature pointer", "[Infrastructure][FcInterface]" )
{
	REQUIRE_NOTHROW( MyLocalFeature->DoNothing() );
	
	GlobalStateReset();
}

TEST_CASE( "Validate initial state of the local Feature Pointer", "[Infrastructure][FcInterface]" )
{
	ValidateInitialState_MyLocalFeature();
}

TEST_CASE( "Validate initial state of the Feature Pointers in Feature.dll", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	ValidateInitialState_FeatureDll();
}

TEST_CASE( "Validate initial state of the Feature Pointers in Feature2.dll", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	ValidateInitialState_Feature2Dll();
}

TEST_CASE( "Query for a unknown feature", "[Infrastructure][FcInterface]" )
{
	const ExpectedFeatureState expected = { "Unk1no2wnF3ea4tur54",
											nullptr,
											FcInterfaceInternals::eFeatureQueryStatus::UNKNOWN_FEATURE };

	ValidateInitialStateOfFeature( expected );
}

TEST_CASE( "Invoke an uninvoked Feature Pointer whose Feature is defined in the same translation unit", "[Infrastructure][FcInterface][!throws]" )
{
	GlobalStateReset();

	CHECK( !FcInterfaceInternals::FeaturePointerUtils::IsReady( MyLocalFeature ) );
	REQUIRE( MyLocalFeature->Get42() == 42 );
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( MyLocalFeature ) );

	REQUIRE_THROWS_WITH( MyLocalFeature->ThrowException(), "Feature Pointer threw this exception" );
	REQUIRE_NOTHROW( MyLocalFeature->DoNothing() );

	REQUIRE( MyLocalFeature->Get43() == 43 );
}

TEST_CASE( "Cross-Compiler: Invoke an uninvoked Feature from a Visual Studio library that is not yet loaded and validate expected results", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	CHECK( !GetModuleHandleA( "Feature" ) );
	CHECK( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );
	REQUIRE_THAT( FcInterfaceInternals::FeaturePointerUtils::GetFeatureName( FeatureInADLL_VS ), Equals( "FeatureInADLL_VS" ) );
	REQUIRE( FeatureInADLL_VS->GetInt() == 42 );
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );
}

TEST_CASE( "Validate results from a duplicate Feature Pointer for the same interface", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL2_VS ) );
	REQUIRE_THAT( FeatureInADLL2_VS->GetFeatureName(), Equals( "FeatureInADLL2_VS" ) );
	REQUIRE( FeatureInADLL2_VS->GetInt() == 24 );
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL2_VS ) );
}

TEST_CASE( "Invoking an unregistred feature should throw", "[Infrastructure][FcInterface][!throws][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_NotRegistered_VS ) );

	const auto expectedExceptionString = "FcInterface Usage Error: Invoked Feature Pointer whose Feature "
		"'FeatureInADLL_NotRegistered_VS' is not available! Status: 'Feature is not Registered'";

	REQUIRE_THROWS_WITH( FeatureInADLL_NotRegistered_VS->GetFeatureName(), expectedExceptionString );
	REQUIRE_THROWS_WITH(*FeatureInADLL_NotRegistered_VS, expectedExceptionString );

	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_NotRegistered_VS ) );
}

TEST_CASE( "Invoking a Feature whose module is not in the application path should still be found by sniffer paths", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInAnotherDLL_VS ) );
	REQUIRE_THAT( FeatureInAnotherDLL_VS->GetFeatureName(), Equals( "FeatureInAnotherDLL_VS" ) );
	REQUIRE( FeatureInAnotherDLL_VS->GetInt() == 44 );
}

TEST_CASE( "Registering a duplicate Feature (same priority) should do nothing",
			"[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	INFO("Invoke to register this feature")
	REQUIRE( FeatureInADLL_VS->GetInt() == 42 );

	INFO("Attempt to load DuplicateFeature.dll to register an already registered feature.")
	INFO("Loading a DLL with a duplicate feature will cause said duplicate to not be registerd.")
	INFO("Originally, we threw which caused LoadLibrary to fail which is not desierable.") // unless, 
	LoadLibraryA( "duplicate/DuplicateFeature" );

	INFO("The original feature shall remain active.")
	REQUIRE( FeatureInADLL_VS->GetInt() == 42 );


	//GlobalStateReset();

	//INFO("Loading DuplicateFeature.dll without registering the feature it's duplicating means it'll work fine")
	//REQUIRE( LoadLibraryA( "duplicate/DuplicateFeature" ) );
	//REQUIRE( FeatureInADLL_VS->GetInt() == 256 );
}

TEST_CASE( "Unloading the Visual Studio library should depopulate Feature Pointers", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	LoadLibraryA( "Feature" );

	REQUIRE_NOTHROW( FeatureInADLL_VS->DoNothing() );
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );
	//CHECK( (*FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS )) );

	REQUIRE_NOTHROW( FeatureInADLL2_VS->DoNothing() );
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL2_VS ) );
	//REQUIRE( (*FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL2_VS )) );

	const HMODULE mod = GetModuleHandleA( "Feature" );

	REQUIRE( mod != 0 );

	FcInterfaceInternals::FeatureIdentity f1 = {"FeatureInADLL_VS", "Feature", 0};
	FcInterfaceInternals::FeatureIdentity f2 = {"FeatureInADLL2_VS", "Feature", 0};

	REQUIRE( QueryFeature( f1 ).status == FcInterfaceInternals::eFeatureQueryStatus::FEATURE_READY );
	REQUIRE( QueryFeature( f2 ).status == FcInterfaceInternals::eFeatureQueryStatus::FEATURE_READY );

	CHECK( FreeLibrary( mod ) );
	REQUIRE( !GetModuleHandleA( "Feature" ) );

	REQUIRE( QueryFeature( f1 ).status == FcInterfaceInternals::eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE );
	REQUIRE( QueryFeature( f2 ).status == FcInterfaceInternals::eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE );
}

TEST_CASE( "Load a DLL that contains a feature with a higher priority than FeatureInADLL_VS.\n"
			"It should override existing Feature Pointers.", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();
	REQUIRE( FeatureInADLL_VS->GetInt() == 42 );

	INFO("Load a DLL that contains a feature with a higher priority than FeatureInADLL_VS")
	INFO("This DLL is in a directory that is not visible to the Sniffer, so it is not loaded automatically")
	
	REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );
	REQUIRE( LoadLibraryA( "override/PriorityOverride" ) );
	REQUIRE( !FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );

	REQUIRE( FeatureInADLL_VS->GetInt() == 555 );
}

TEST_CASE( "Dynamic Feature Pointer Test", "[Infrastructure][FcInterface][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	CDynamicFeaturePointer<IFLOWCALFeatureInADLL_VS> fp( "FeatureInADLL_VS" );

	REQUIRE( fp->GetInt() == 42 );
	fp.SetPluginFeatureName( "FeatureInADLL2_VS" );
	REQUIRE( fp->GetInt() == 24 );

	//REQUIRE_THROWS_WITH( fp.SetPluginFeatureName( "MyLocalFeature" ), "CDynamicFeaturePointer type mismatch!" );
}

#if NDEBUG // benchmark tests should only be ran in Release mode
struct StringPool final
{
	const char* Track( const char* str )
	{
		return stringPool.insert( str ).first->c_str();
	}

	const char* GenerateUniqueString()
	{
		static long long count = 0;
		std::string str = std::to_string( ++count );

		return Track( str.c_str() );
	}

private:
	std::unordered_set<std::string> stringPool;
};

StringPool& Pool()
{
	StringPool* pool = new StringPool;
	return *pool;
}

struct CFeatureRep final
{
	FcInterfaceInternals::FeatureRegistrationHelper helper;

	static void* wut()
	{
		static CTestFcInterface concrete;
		return &concrete;
	}

	CFeatureRep() : helper( {Pool().Track( Pool().GenerateUniqueString() ), FcInterface_GetThisModuleName(), 0, &wut} )
	{
	}
};

typedef std::chrono::high_resolution_clock HRC;
typedef std::chrono::nanoseconds ns;

TEST_CASE( "Feature Registration Stress Test: Register Features", "[Infrastructure][FcInterface][benchmark][HasExternalDependency][!hide]" )
{
	GlobalStateReset();

	// 10000 features is a lot, so this should be sufficient for scalability testing.
	const size_t featureCount =	10000;

	CAPTURE( featureCount );
	INFO( "Create " << featureCount << " unique features." )

	static std::vector<CFeatureRep> features;
	features.reserve( featureCount );

	ns total( 0 );
	size_t oneSecondCount = 0;
	for( size_t i = 0; i < featureCount; ++i )
	{
		HRC::time_point start = HRC::now();
		features.emplace_back();
		HRC::time_point end = HRC::now();
		total += std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

		if( i > 0 && i % 5000 == 0 )
		{
			WARN( "Average Feature register cost (interval): " << (total / featureCount).count() << "ns" );
		}

		if( oneSecondCount == 0 && std::chrono::duration_cast<std::chrono::seconds>( total ) >= std::chrono::seconds( 1 ) )
		{
			oneSecondCount = i;
		}
	}

	WARN( "1 second total Overhead count: " << oneSecondCount << " Features" );
#ifdef NDEBUG
	CHECK( (oneSecondCount == 0 || oneSecondCount > 30'000) );
#endif

	WARN( "Average Feature register cost: " << std::setprecision( 4 ) << ( total / featureCount ).count() << "ns" );
}

// Not scientific, just very rough ballpark estimates
TEST_CASE( "Feature Pointer Stress Test: Creation/Invocation overhead vs normal virtual calls", "[Infrastructure][FcInterface][HasExternalDependency][benchmark][!hide]" )
{
	// Pre C++17 we'll have one FP per translaton unit, C++17 and later it's one per binary
	// This number is based on a massive 25,000 translation unit program, if it performs well here, we should be OK.
	const size_t numEntries = 25000;

	// Create a bunch of FPs
	std::vector<FcInterfaceInternals::CFeaturePointer<IFLOWCALFeatureInADLL_VS*>>	featurepointers;
	featurepointers.reserve( numEntries );

	ns total = ns::zero();

	{
		// Ensure loaded to not measure initial cost & measure it
		HRC::time_point start = HRC::now();
		FeatureInADLL_VS->DoNothing();
		HRC::time_point end = HRC::now();
		total += end - start;
		REQUIRE( FcInterfaceInternals::FeaturePointerUtils::IsReady( FeatureInADLL_VS ) );
		WARN( "Initial cost (very small module): " << total.count() << "ns" );
		total = ns::zero();
	}

	// Get average time for a normal virtual dispatch call across DLLs
	for( int i = 0; i < numEntries; i++ )
	{
		HRC::time_point start = HRC::now();
		FeatureInADLL_VS->DoNothing();
		HRC::time_point end = HRC::now();
		total += end - start;

		if( i == 0 )
		{
			WARN( "Initial query time - virtual call: " << total.count() << "ns" );
		}
	}
	const auto averageQueryTime_NonFP = total.count() / numEntries;
	WARN( "Average query time - virtual call: " << averageQueryTime_NonFP << "ns" );

	// Get average initial FP invocation cost (FP has to Populate itself)
	total = ns::zero();
	for( int i = 0; i < numEntries; i++ )
	{
		featurepointers.emplace_back("FeatureInADLL_VS");
		HRC::time_point start = HRC::now();
		featurepointers[i]->DoNothing();
		HRC::time_point end = HRC::now();
		total += end - start;

		if( i == 0 )
		{
			WARN( "Initial query time - FP (Initial): " << total.count() << "ns" );
		}
	}
	const auto averageQueryTime_FP_initial = total.count() / numEntries;
	WARN( "Average query time - FP (Initial): " << averageQueryTime_FP_initial << "ns" );

	total = ns::zero();
	for( int i = 0; i < numEntries; i++ )
	{
		HRC::time_point start = HRC::now();
		featurepointers[i]->DoNothing();
		HRC::time_point end = HRC::now();
		total += end - start;

		if( i == 0 )
		{
			WARN( "Initial query time - FP (Subsequent): " << total.count() << "ns" );
		}
	}
	const auto averageQueryTime_FP = total.count() / numEntries;
	WARN( "Average query time - FP (Subsequent): " << averageQueryTime_FP << "ns" );

	const auto InitialFactor = 2.0, SubsequentFactor = 1.1;

	const auto nsToleranceForInitial = 25000;
	const auto nsToleranceForSubsequent = 50;

	CAPTURE( averageQueryTime_NonFP * InitialFactor );
	CAPTURE( averageQueryTime_FP_initial );
	CAPTURE( averageQueryTime_NonFP + nsToleranceForInitial );
	CAPTURE( averageQueryTime_FP_initial );

	WARN( "Initial FP Overhead: " << std::setprecision(4) << (((averageQueryTime_FP_initial / (float)averageQueryTime_NonFP) * 100) - 100) << "%" );
	REQUIRE( (averageQueryTime_NonFP * InitialFactor > averageQueryTime_FP_initial || averageQueryTime_NonFP + nsToleranceForInitial > averageQueryTime_FP_initial) );

	CAPTURE( averageQueryTime_NonFP * SubsequentFactor );
	CAPTURE( averageQueryTime_FP );
	CAPTURE( averageQueryTime_NonFP + nsToleranceForSubsequent );
	CAPTURE( averageQueryTime_FP );

	WARN( "Subsequent FP Overhead: " << std::setprecision(4) << ( ( averageQueryTime_FP / (float)averageQueryTime_NonFP ) * 100 ) - 100 << "%" );
	REQUIRE( (averageQueryTime_NonFP * SubsequentFactor > averageQueryTime_FP || averageQueryTime_NonFP + nsToleranceForSubsequent > averageQueryTime_FP) );
}
#endif