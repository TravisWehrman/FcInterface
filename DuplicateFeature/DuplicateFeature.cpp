// Duplicate this Feature
#include "../FcFeature/FcFeature.h"

class CDuplicateFeature final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		return "FeatureInAnotherDLL_VS_PriorityOverride";
	}

	int GetInt() override
	{
		return 256;
	}	

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER( CDuplicateFeature, IFLOWCALFeatureInADLL_VS, FeatureInADLL_VS );