#ifndef FcInterfaceH
#define FcInterfaceH
#pragma once

// See readme.md for more information.

#include <stdexcept>
#include <string>
#include <vector>

#include "internal/platform.h"

namespace FcInterfaceInternals
{
using FetchFeatureFn = void*(*)();

#if defined(_MSC_VER)
#define EL_REPOFETCHER_EXPORT_NAME		_ELInterface
#define EL_REPOFETCHER_EXPORT_PREFIX
#elif defined(__BORLANDC__)
#define EL_REPOFETCHER_EXPORT_NAME		ELInterface
#define EL_REPOFETCHER_EXPORT_PREFIX	"_"
#endif

// data to uniquely identify a specific feature instance
struct FeatureIdentity final
{
	const char*			featureName;
	const char*			moduleName;
	int					priority;

	enum : char
	{
		TOP_PRIORITY = -1
	};
};

// For when we need to own the strings
struct FeatureIdentityInternal final
{
	std::string			featureName;
	std::string			moduleName;
	int					priority;
};

struct FeatureState final
{
	FeatureIdentity		id;
	FetchFeatureFn		fn;
};

struct IFeaturePointer
{
	virtual const char*		GetFeatureName() const = 0;

protected:

	friend struct CFcInterface;

	virtual bool			Populate() = 0;

	// Causes the next invocation of this feature pointer to requery for the feature
	virtual void			Reset() = 0;
};

enum class eFeatureQueryStatus : char // Must synchronize underlying type between compilers
{
	FEATURE_READY,
	UNKNOWN_FEATURE,				// Feature is unknown and not found by FeatureSniffer. Likely missing FCINTERFACE_REGISTER() for Feature or a missing path in FCI_SNIFFER_PATHS.
	KNOWN_FEATURE_BUT_UNAVAILABLE,	// Feature was identified by FeatureSniffer but is not currently loaded. Invocation of FeaturePointer will attempt load.
};

inline const char* FeatureStatusToString( eFeatureQueryStatus status )
{
	switch(status)
	{
	case eFeatureQueryStatus::FEATURE_READY:
		return "Feature is Ready";
	case eFeatureQueryStatus::UNKNOWN_FEATURE:
		return "Feature is not Registered";
	case eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE:
		return "Feature is Registered but module is not loaded";
	}
}

struct FeatureQuery final
{
	operator bool() const { return fp != nullptr; }

	FeatureIdentity		id;
	void*				fp;
	eFeatureQueryStatus status;
};

struct IFcInterface
{
	virtual void FCI_CALLCONV			RegisterFeature( const FeatureState& ) = 0;
	virtual void FCI_CALLCONV			UnregisterFeature( const FeatureIdentity& ) = 0;

	virtual void FCI_CALLCONV			RegisterFeaturePointer( IFeaturePointer* ) = 0;
	virtual void FCI_CALLCONV			UnregisterFeaturePointer( IFeaturePointer* ) = 0;

	virtual void FCI_CALLCONV			LaunchSniffer() = 0;
	//virtual void FCI_CALLCONV			ValidateAllFeaturePointers() = 0; // Ensure all featurePointers are not missing their respective Features

	virtual bool FCI_CALLCONV			EnsureModuleLoaded( FeatureQuery ) = 0; // Takes FeatureQuery to avoid duplicate queries
	virtual FeatureQuery FCI_CALLCONV	QueryFeature( const FeatureIdentity& ) = 0;
};
using GetFcInterfaceFn = IFcInterface*(FCI_CALLCONV*)();

#if defined(EL_REPO_LOCATION)
#define EL_REPO_MODULE_NAME FCI_TO_STRING(EL_REPO_MODULE_NAME_NAME)
#else
#define EL_REPO_MODULE_NAME "EasyLink"
#endif

#define EL_MODULE_FEATURE_PREFIX ELCF_

struct CModuleRepoMock final : public IFcInterface
{
	void FCI_CALLCONV RegisterFeature( const FeatureState& state ) override
	{
		pendingFeatureRegistrations.push_back( state );
	}

	void FCI_CALLCONV UnregisterFeature( const FeatureIdentity& feature ) override
	{
		// May be called without registering cache
		pendingFeatureRegistrations.erase( std::find_if( std::begin(pendingFeatureRegistrations), std::end(pendingFeatureRegistrations), [=](const FeatureState& entry)
		{
			const bool isFeature = std::strcmp( entry.id.featureName, feature.featureName ) == 0;
			if( feature.priority == FeatureIdentity::TOP_PRIORITY )
			{
				return isFeature;
			}
			else
			{
				return isFeature && feature.priority == entry.id.priority;
			}
		} ), std::end(pendingFeatureRegistrations) );
	}

	void FCI_CALLCONV RegisterFeaturePointer( IFeaturePointer* featurePointer ) override
	{
		pendingFeaturePointerRegistrations.push_back( featurePointer );
	}

	void FCI_CALLCONV UnregisterFeaturePointer( IFeaturePointer* featurePointer ) override
	{
		pendingFeaturePointerRegistrations.erase( std::find_if( std::begin(pendingFeaturePointerRegistrations), std::end(pendingFeaturePointerRegistrations), [featurePointer](const IFeaturePointer* entry)
		{
			return entry == featurePointer;
		} ), std::end(pendingFeaturePointerRegistrations) );
	}

	void FCI_CALLCONV LaunchSniffer() override {}
	//void FCI_CALLCONV ValidateAllFeaturePointers() override {}
	bool FCI_CALLCONV EnsureModuleLoaded( FeatureQuery ) override { return false; }
	FeatureQuery FCI_CALLCONV QueryFeature( const FeatureIdentity& feature ) override { return { feature, nullptr, eFeatureQueryStatus::UNKNOWN_FEATURE }; }

	void RegisterPendingEntries( IFcInterface* repo )
	{
		for( auto entry : pendingFeatureRegistrations )
		{
			repo->RegisterFeature( entry );
		}

		pendingFeatureRegistrations.clear();

		for( auto featurePointer : pendingFeaturePointerRegistrations )
		{
			repo->RegisterFeaturePointer( featurePointer );
		}

		pendingFeaturePointerRegistrations.clear();
	}

	void Reset()
	{
		pendingFeatureRegistrations.clear();
		pendingFeaturePointerRegistrations.clear();
	}

private:
	std::vector<FeatureState>		pendingFeatureRegistrations;
	std::vector<IFeaturePointer*>	pendingFeaturePointerRegistrations;
};

// Module state data so we can reset it (Primarily used for testing)
// All inline/static variables in this header should go in here unless they are guaranteed to not be different after each initialization (e.g. module names)
struct CModuleState final
{
	GetFcInterfaceFn repoInterface = nullptr;
	CModuleRepoMock mockRepo;
	bool frameworkInit = false;

	void Reset()
	{
		mockRepo.Reset();
		repoInterface = nullptr;
		frameworkInit = false;
	}
};

inline CModuleState& GetModuleState()
{
	static CModuleState state;
	return state;
}

inline GetFcInterfaceFn GetRepoFetcherFn()
{
	if( GetModuleState().repoInterface )
	{
		return GetModuleState().repoInterface;
	}

	auto QueryRepoFetcher = []( const char* moduleName )
	{
		return reinterpret_cast<GetFcInterfaceFn>(FcGetProcAddress(moduleName, EL_REPOFETCHER_EXPORT_PREFIX FCI_TO_STRING(EL_REPOFETCHER_EXPORT_NAME)));
	};

	auto RepoModuleName = EL_REPO_MODULE_NAME;
	GetModuleState().repoInterface = QueryRepoFetcher( RepoModuleName );

	if( !GetModuleState().repoInterface )
	{
		GetModuleState().repoInterface = QueryRepoFetcher( nullptr );
	}

	return GetModuleState().repoInterface;
};
inline IFcInterface* GetRepoInstance()
{
	GetFcInterfaceFn repoFetcher = GetRepoFetcherFn();

	// The Mock repo allows processes that don't use EasyLink to load these modules
	// and enables registration before the repository is available (Since it's not safe to load modules during global construction)
	// In this case, the sniffer will call on the module to reattempt registration.
	// Each library has a single mock instance.
	if( !repoFetcher )
	{
		return &GetModuleState().mockRepo;
	}

	auto repo = repoFetcher();

	GetModuleState().mockRepo.RegisterPendingEntries( repo );

	return repo;
}

struct FcInterfaceInvokedUnavailableFeatureException final : public std::logic_error
{
	FcInterfaceInvokedUnavailableFeatureException( const IFeaturePointer* featurePointer )
		: std::logic_error("FcInterface Usage Error"), msg("FcInterface Usage Error: ")
	{
		const auto featureStatus = GetRepoInstance()->QueryFeature( {featurePointer->GetFeatureName(), nullptr, FeatureIdentity::TOP_PRIORITY });
		const auto err = std::string("Invoked Feature Pointer whose Feature '") + featurePointer->GetFeatureName() +
			"' is not available! Status: '" + FeatureStatusToString( featureStatus.status ) + "'";
		msg.append(err);
	}

	const char* what() const override
	{
		return msg.c_str();
	}

private:

	std::string msg;
};

struct FeatureRegistrationHelper final
{
	explicit FeatureRegistrationHelper( FeatureState State )
		: state(State)
	{
		Register();
	};

	void Register()
	{
		GetRepoInstance()->RegisterFeature( state );
	}

	~FeatureRegistrationHelper()
	{
#if defined(__BORLANDC__) // Handle non-standard _exit() issues
		__try
		{
			GetRepoInstance()->UnregisterFeature( state.id );
		}
		__except(EXCEPTION_EXECUTE_HANDLER) {}
#else
		GetRepoInstance()->UnregisterFeature( state.id );
#endif
	}

private:
	FeatureState state;
};

template<typename FeatureType>
FeatureQuery FeaturePointerPopulator( FeatureType* ifaceOut, const FeatureIdentity& feature )
{
	FeatureQuery Feature = GetRepoInstance()->QueryFeature( feature );

	*ifaceOut = reinterpret_cast<FeatureType>(Feature.fp);
	return Feature;
}

template<typename FeatureType>
FeatureQuery FeaturePointerPopulator( FeatureType* ifaceOut, const FeatureQuery& feature )
{
	*ifaceOut = reinterpret_cast<FeatureType>(feature.fp);
	return feature;
}

inline void AttemptExternalRepoLoad()
{
	FcLoadLibraryIfNotLoaded( EL_REPO_MODULE_NAME );
}

inline void FrameworkInit()
{
	if( !GetModuleState().frameworkInit )
	{
		GetModuleState().frameworkInit = true;
		AttemptExternalRepoLoad();
		GetRepoInstance()->LaunchSniffer();
	}
}

template<typename FPType> struct CFeaturePointer;
namespace FeaturePointerUtils
{
inline void Register( IFeaturePointer& featurePointer )
{
	GetRepoInstance()->RegisterFeaturePointer( &featurePointer );
}

template<typename FPType>
void Reset( CFeaturePointer<FPType>& featurePointer )
{
	featurePointer.fp = nullptr;
}

template<typename FPType>
const bool IsReady( const CFeaturePointer<FPType>& featurePointer )
{
	return featurePointer.fp != nullptr && (*featurePointer.fp) != nullptr;
}

template<typename FPType>
const char* GetFeatureName( const CFeaturePointer<FPType>& featurePointer )
{
	return featurePointer.featureName;
}
}

template<typename FPType>
struct CFeaturePointer : public IFeaturePointer
{
	static_assert(std::is_pointer<FPType>::value, "must take in a pointer type");

	FPType operator->() { return Get();	}
	operator FPType() { return Get(); }

	explicit CFeaturePointer( const char* FeatureName )
		: featureName(FeatureName)
	{
		GetRepoInstance()->RegisterFeaturePointer( this );
	}

	~CFeaturePointer()
	{
#if defined(__BORLANDC__) // Handle non-standard _exit() issues
		// Heap memory (repo) may be deallocated before global destruction begins
		__try
		{
			GetRepoInstance()->UnregisterFeaturePointer( this );
		}
		__except(EXCEPTION_EXECUTE_HANDLER) {}
#else
		GetRepoInstance()->UnregisterFeaturePointer( this );
#endif
	}

	friend void FeaturePointerUtils::Reset<FPType>( CFeaturePointer<FPType>& );
	friend const bool FeaturePointerUtils::IsReady<FPType>( const CFeaturePointer<FPType>& );
	friend const char* FeaturePointerUtils::GetFeatureName<FPType>( const CFeaturePointer<FPType>& );

protected:

	FPType Get()
	{
		if( !fp || !*fp )
		{
			if( !Populate() )
			{
				throw FcInterfaceInvokedUnavailableFeatureException( this );
			}
		}

		return *fp;
	}

	bool Populate() override
	{
		FrameworkInit();

		const FeatureIdentity id = {featureName, nullptr, FeatureIdentity::TOP_PRIORITY};

		const auto feature = FeaturePointerPopulator( &fp, id );

		if( feature )
		{
			return feature;
		}

		if( GetRepoInstance()->EnsureModuleLoaded( feature ) )
		{
			return FeaturePointerPopulator( &fp, id );
		}

		return feature;
	}

	const char* GetFeatureName() const override
	{
		return featureName;
	}

	void Reset() override
	{
		fp = nullptr;
	}

	const FPType* fp = nullptr;
	const char* featureName = nullptr;
};
};

// A Feature Pointer that can change Features at runtime of the same interface type (i.e. a plugin handler)
template<typename FPType>
struct CDynamicFeaturePointer final : public FcInterfaceInternals::CFeaturePointer<FPType*>
{
	CDynamicFeaturePointer( const std::string& FeatureName )
		: FcInterfaceInternals::CFeaturePointer<FPType*>( nullptr )
	{
		SetPluginFeatureName( FeatureName );
	}
	CDynamicFeaturePointer( const CDynamicFeaturePointer& ) = delete;

	void SetPluginFeatureName( const std::string& FeatureName )
	{
		this->fp = nullptr;
		sFeatureName = FeatureName;
		this->featureName = sFeatureName.c_str();
	}

	FPType* operator->() { return this->Get(); }

private:

	std::string sFeatureName;
};

inline const char* FcInterface_GetThisModuleName()
{
	static std::string dllName;
	if( dllName.empty() )
	{
		dllName = FcGetThisModuleName( &FcInterface_GetThisModuleName );
	}

	return dllName.c_str();
}

// == Internal ==
#define Fc_FCINTERFACE_INTERNAL_VALIDATIONS(abstractClass) \
	static_assert(std::is_abstract<abstractClass>::value, "FcInterface class " #abstractClass " is not abstract!");
	//static_assert(sizeof(abstractClass) == sizeof(void*), "FcInterface class " #abstractClass " should not have data members (vtable ptr only)!"); // May have multiple vtable ptrs

// Declare a Feature whose interface already exists
// Can also add this after FCINTERFACE_DECLARE to declare a different concrete that uses the same interface
#define FCINTERFACE_DECLARE_EXISTING(featureClassName, featureName) \
EL_SHARED_LINKAGE FcInterfaceInternals::CFeaturePointer<featureClassName*> featureName( FCI_TO_STRING( featureName ) );

#define Fc_FCINTERFACE_INTERNAL_FP(featureClassName, featureName) \
struct featureClassName##Feature final : public FcInterfaceInternals::CFeaturePointer<featureClassName*>{ \
featureClassName##Feature() : FcInterfaceInternals::CFeaturePointer<featureClassName*>( FCI_TO_STRING( featureName ) ) {} \
featureClassName* operator()() { return Get(); } };

//--------------------------------------------------//
// -- Helper macros; use these to use FcInterface --
//--------------------------------------------------//

// == Step 1: Declaration ==
#define FCINTERFACE_DECLARE(featureClassName, featureName, interfaceBody) \
struct featureClassName interfaceBody; /* Declare Feature interface */ \
Fc_FCINTERFACE_INTERNAL_VALIDATIONS(featureClassName) \
FCINTERFACE_DECLARE_EXISTING(featureClassName, featureName); /* Local Feature Pointer */ \
Fc_FCINTERFACE_INTERNAL_FP( featureClassName, featureName )

// Declare an interface but do not create a feature pointer accessor
#define FCINTERFACE_DECLARE_NOFP(featureClassName, featureName, interfaceBody) \
struct featureClassName interfaceBody; /* Declare Feature interface */ \
Fc_FCINTERFACE_INTERNAL_VALIDATIONS(featureClassName) \
Fc_FCINTERFACE_INTERNAL_FP(featureClassName, featureName)

// Also provide a base interface to derive from
#define FCINTERFACE_DECLARE_DERIVED(featureClassName, BaseInterface, featureName, interfaceBody) \
struct featureClassName : public BaseInterface interfaceBody; /* Declare Feature interface */ \
Fc_FCINTERFACE_INTERNAL_VALIDATIONS(featureClassName) \
FCINTERFACE_DECLARE_EXISTING(featureClassName, featureName); /* Local Feature Pointer */

#define ELI_CONCAT(a, b) a ## b
#define EL_CONCAT(a, b) ELI_CONCAT(a, b)

// You provide an existing concrete feature instance
#define FCINTERFACE_REGISTER_GLOBALVAR(concreteClass, abstractClass, featureName, featurePriority, globalVar) \
	Fc_FCINTERFACE_INTERNAL_VALIDATIONS( abstractClass ) \
	/*concreteClass is only used here for a unique name, this way we can declare multiple instances from the same interface in the same TU*/ \
	static FcInterfaceInternals::FeatureRegistrationHelper Fc_g_Create##concreteClass##abstractClass##_reg( { \
	{FCI_TO_STRING(featureName), FcInterface_GetThisModuleName(), featurePriority}, []()->void* { return &globalVar; } } ); \
	extern "C" __declspec( dllexport ) void FCI_CALLCONV EL_CONCAT(EL_MODULE_FEATURE_PREFIX, featurePriority##_##featureName)() { FcInterfaceInternals::GetRepoInstance(); } /*These exports double as a way to advertise features for the sniffer, and a way to reattempt registration when invoked. GetRepoInstance() will register pending entities. */

// == Step 2: Registration ==
// Same as above but creates the concrete feature instance for you
#define FCINTERFACE_REGISTER(concreteClass, abstractClass, featureName) \
	static concreteClass fcinterface_##concreteClass##_global; \
	FCINTERFACE_REGISTER_GLOBALVAR(concreteClass, abstractClass, featureName, 0, fcinterface_##concreteClass##_global)

// Define a priority, allows features of the same name to override other features
// Useful for mocking in unit testing
#define FCINTERFACE_REGISTER_PRIORITY(concreteClass, abstractClass, featureName, featurePriority) \
	static concreteClass fcinterface_##concreteClass##_global; \
	FCINTERFACE_REGISTER_GLOBALVAR(concreteClass, abstractClass, featureName, featurePriority, fcinterface_##concreteClass##_global)
#endif // FcInterfaceH