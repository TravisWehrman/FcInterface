#pragma once

#include <string>

#include "static_vector.h"

struct CFeature final
{
	void*					fn;			// Is nullptr if the feature is not available
	FeatureIdentityInternal id;
};

struct FeatureRepository final
{
	FeatureRepository() {}
	FeatureRepository( const FeatureRepository& ) = delete;

	// Features live in static memory as Feature Pointers rely on their memory location to check for Feature availability.
	// This removes the need for the repo to track feature pointers (has O(n2) tracking cost)
	void					AppendFeature( const CFeature& feature );

	// Find a specific feature instance (featurename + priority is a unique identifier)
	// Providing only the feature name will give the highest priority feature
	CFeature*				FindFeature( const FeatureIdentity& feature );

	// Multiple feature instances may exist, return in order of priority
	std::vector<CFeature*>	FindFeature( const char* featureName );

private:

	static_vector<CFeature, 2048>		featureBenches;
};

namespace Repository
{
	FeatureRepository& Get();
}