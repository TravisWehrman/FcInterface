#pragma once

#include "FcInterface.h"

// Declare your abstract class with this helper macro
FCINTERFACE_DECLARE( IFLOWCALFeatureInADLL_VS, FeatureInADLL_VS,
{
	virtual const char* GetFeatureName() = 0;
	virtual int GetInt() = 0;
	virtual void DoNothing() = 0;
});

FCINTERFACE_DECLARE_EXISTING( IFLOWCALFeatureInADLL_VS, FeatureInADLL2_VS )
FCINTERFACE_DECLARE_EXISTING( IFLOWCALFeatureInADLL_VS, FeatureInADLL_NotRegistered_VS )
FCINTERFACE_DECLARE_EXISTING( IFLOWCALFeatureInADLL_VS, FeatureInAnotherDLL_VS )

static struct CMyFeatures final
{
	CMyFeatures* operator->() { return this; }
	IFLOWCALFeatureInADLL_VS* FeatureInADLL() { return FeatureInADLL_VS; }
	IFLOWCALFeatureInADLL_VS* FeatureInADLL2() { return FeatureInADLL2_VS; }
} MyFeatures;