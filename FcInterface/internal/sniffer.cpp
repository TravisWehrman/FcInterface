#include "sniffer.h"
#include "../FcInterface.h"
#include "platform_internal.h"

using namespace Sniffer;

namespace
{
	void SetSnifferDirsToAbsPath( fs::path exeFolderPath )
	{
		for( auto& dir : GetSnifferDirs().dirs )
		{
#if FCI_CPP17_SUPPORT || (( BOOST_VERSION / 100 ) % 1000) > 55
//#error "FcInterface: Detected newer version of boost or C++17, Please use this method and remove the other"
			dir = exeFolderPath.append( dir.string() ).make_preferred();
#else
			dir = exeFolderPath.string() + dir.string();
			dir = dir.make_preferred();
#endif
		}
	}

	std::string ReconstructFeatureExportSignatureFromFeatureName( const char* featureName, int priority )
	{
		std::string exportName = module_feature_prefix;
		exportName.append( std::to_string(priority) + "_" );
		exportName.append( featureName );
		return exportName;
	}

	// Modules that load before the repository is loaded may not have registered if no Feature Pointers were invoked in said module.
	void EnsureModuleRegistered( const FeatureIdentity& id )
	{
		// This is only applicable for modules that are already loaded
		if( !FcIsModuleLoaded( id.moduleName ) )
			return;

		auto exportName = ReconstructFeatureExportSignatureFromFeatureName( id.featureName, id.priority );

		auto QueryExport = [](const char* moduleName, const char* exportName )
		{
			return reinterpret_cast<void(FCI_CALLCONV*)()>(FcGetProcAddress( moduleName, exportName ) );
		};

		auto exportFn = QueryExport( id.moduleName, exportName.c_str() );

		if( exportFn )
		{
			// Invoking any of a module's feature exports will trigger a reregistration
			exportFn();
			return;
		}

#if __BORLANDC__ // By default, BCC prepends _
		exportName.insert (0, "_" );
		exportFn = QueryExport( id.moduleName, exportName.c_str() );

		if( exportFn )
		{
			exportFn();
			return;
		}
#endif
	}

	void SniffDirForFeatures( CFeatureSniffer& sniffer, const fs::path& dir )
	{
		if( !fs::is_directory( dir ) )
			return;

#if FCI_CPP17_SUPPORT || (( BOOST_VERSION / 100 ) % 1000) > 55
//#error "FcInterface: Detected newer version of boost or C++17, Please use this method and remove the other"
		for( const auto entry : fs::directory_iterator( dir ) )
		{
#else
		const fs::directory_iterator end_iter;
		for(fs::directory_iterator dir_iter(dir); dir_iter != end_iter; ++dir_iter )
		{
			auto entry = *dir_iter;
#endif
			std::string extension = entry.path().extension().string();
			std::transform( extension.begin(), extension.end(), extension.begin(), ::tolower );

			if( extension != ".dll" && extension != ".bpl" )
				continue;

			std::string dllName = entry.path().filename().string();
			std::transform( dllName.begin(), dllName.end(), dllName.begin(), ::tolower );

			const bool isModuleLoaded = FcIsModuleLoaded( dllName.c_str() );

			const std::string& dllPath = entry.path().string();
			const auto advertisedFeatures = GatherFeaturesFromModule( dllPath.c_str() );

			if( !isModuleLoaded ) // If already loaded, then a registration attempt was already made
			{
				for( const auto& exportSig : advertisedFeatures )
				{
					sniffer.Add( { exportSig.first, dllName, exportSig.second } );
				}
			}

			if( !advertisedFeatures.empty() )
			{
				EnsureModuleRegistered( {advertisedFeatures.front().first.c_str(), dllName.c_str(), advertisedFeatures.front().second} );
			}
		}
	}
}

namespace Sniffer
{
	CSnifferDirectories& GetSnifferDirs()
	{
		static CSnifferDirectories snifferDirs;
		return snifferDirs;
	}

	CFeatureSniffer Sniff()
	{
		CFeatureSniffer sniffer;

		const auto exeFolderPath = GetEXEDirectory();

		SetSnifferDirsToAbsPath( exeFolderPath );
		GetSnifferDirs().dirs.push_back( exeFolderPath );

		for( const auto& dir : GetSnifferDirs().dirs )
		{
			SniffDirForFeatures( sniffer, dir );
		}
		return sniffer;
	}
}

void CFeatureSniffer::Add( const FeatureIdentityInternal& feature )
{
	featureSnifferRepo.push_back( feature );
}

void CFeatureSniffer::RegisterFindings() const
{
	for( const auto& feature : featureSnifferRepo )
	{
		FcInterfaceInternals::GetRepoInstance()->RegisterFeature( {{feature.featureName.c_str(), feature.moduleName.c_str(), feature.priority}, nullptr} );
	}
}

CSnifferDirectories::CSnifferDirectories()
{
#if defined(FCI_SNIFFER_PATHS)
	std::string split;
	std::istringstream ss( FCI_SNIFFER_PATHS );
	while( std::getline( ss, split, ',' ) )
	{
		dirs.push_back( split );
	}
#endif
}