// Overrides are based on existing Feature Declarations
#include "../FcFeature/FcFeature.h"

class CPriorityOverrideFeature final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		return "FeatureInAnotherDLL_VS_PriorityOverride";
	}

	int GetInt() override
	{
		return 554;
	}	

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER_PRIORITY( CPriorityOverrideFeature, IFLOWCALFeatureInADLL_VS, FeatureInADLL_VS, 1 );

class CPriorityOverrideFeature2 final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		return "FeatureInAnotherDLL_VS_PriorityOverride2";
	}

	int GetInt() override
	{
		return 555;
	}	

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER_PRIORITY( CPriorityOverrideFeature2, IFLOWCALFeatureInADLL_VS, FeatureInADLL_VS, 1337 );