#include "../FcFeature/FcFeature.h"

class CConcreteFeature2 final : public IFLOWCALFeatureInADLL_VS
{
	const char* GetFeatureName() override
	{
		return "FeatureInAnotherDLL_VS";
	}

	int GetInt() override
	{
		return 44;
	}

	void DoNothing() override
	{
	}
};

FCINTERFACE_REGISTER( CConcreteFeature2, IFLOWCALFeatureInADLL_VS, FeatureInAnotherDLL_VS );