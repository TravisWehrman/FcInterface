#pragma once

#include "filesystem.h"

#include <string>
#include <vector>

struct CFeatureSniffer final
{
	CFeatureSniffer() { featureSnifferRepo.reserve( 2048 ); }
	void Add( const FeatureIdentityInternal& feature );
	void RegisterFindings() const;

private:

	std::vector<FeatureIdentityInternal> featureSnifferRepo;
};


namespace Sniffer
{
	CFeatureSniffer Sniff();

	struct CSnifferDirectories final
	{
		CSnifferDirectories();
		PathList dirs;
	};

	CSnifferDirectories& GetSnifferDirs();
}