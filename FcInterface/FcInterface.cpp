#include "FcInterface.h"

#include <algorithm>
#include <string>
#include <stdexcept>
#include <vector>

#include <Windows.h>

using namespace FcInterfaceInternals;

#include "internal/repository.h"
#include "internal/sniffer.h"
#include "internal/modulesearchpath.h"

namespace FcInterfaceInternals
{
	EL_EXPORT IFcInterface* FCI_EXPORT_CALLCONV EL_REPOFETCHER_EXPORT_NAME();

	// Exception used for edge cases that shouldn't ever happen
	// I would still like to know if this happens though
	struct FcInterfaceUnxpectedBehaviorException final : public std::logic_error
	{
		FcInterfaceUnxpectedBehaviorException( const std::string& Message )
			: std::logic_error(Message.c_str()), msg("FcInterface Internal Error: ")
		{
			msg.append(Message);
		}

		const char* what() const override
		{
			return msg.c_str();
		}

	private:

		std::string msg;
	};

	struct CFcInterface final : public IFcInterface
	{
		bool DoesFeatureSupersedeAnExistingFeature( std::vector<CFeature*> features, const FeatureState& state )
		{
			for( auto feature : features )
			{
				if( feature->id.priority < state.id.priority )
				{
					return true;
				}
			}

			return false;
		}

		bool IsSameFeatureInstance( CFeature* f1, const FeatureState& f2 ) const
		{
			return std::strcmp( f1->id.featureName.c_str(), f2.id.featureName ) == 0 &&
				std::strcmp( f1->id.moduleName.c_str(), f2.id.moduleName ) == 0 &&
				f1->id.priority == f2.id.priority;
		}

		// NOTE: throwing here kills perf for some reason (related to LoadLibrary)
		void FCI_CALLCONV RegisterFeature( const FeatureState& state ) override
		{
			std::vector<CFeature*> features = Repository::Get().FindFeature( state.id.featureName );
			
			if( !features.empty() )
			{
				// If this feature has a higher priority than one already registered, go ahead and register it
				// This means any unregistered feature pointers will use this feature instead of the earlier ones
				if( DoesFeatureSupersedeAnExistingFeature( features, state ) )
				{
					// We are about to register a priority feature, reset all applicable FeatirePointers
					for( auto featurePointer : featurePointers )
					{
						if( std::strcmp( featurePointer->GetFeatureName(), state.id.featureName ) == 0 )
						{
							featurePointer->Reset();
						}
					}
				}
				else
				{
					// NOTE: We probably shouldn't throw an exception during library load
					// Causing LoadLibrary to fail may be a bit extreme, maybe we should just ignore it and stick with the initial feature
					for( auto feature : features )
					{
						// A null fn means it was unregistered and should be restored
						if( !feature->fn && IsSameFeatureInstance( feature, state ) )
						{
							// Restore original feature or populate for Sniffer
							feature->fn = state.fn ? state.fn() : nullptr;
						}
						//throw "TEST";
						/*//if( feature->fn && state.fn ) // nullptr fn means we're registering from the FeatureSniffer
						{
							// Feature already exists, likely legit code duplicate.
							const std::string str = "FcInterface: Error: tried to register duplicate feature '" + std::string( state.featureName ) + " from " + std::string( state.moduleName ) + "'\n";
							throw str;
						}*/

						//feature->fn = state.fn ? state.fn() : nullptr;
					}

					return; // don't register, EnsureModuleLoaded() will throw duplicateFeature error
				}
			}

			Repository::Get().AppendFeature( { state.fn ? state.fn() : nullptr, state.id.featureName, state.id.moduleName, state.id.priority } );
		}

		void FCI_CALLCONV UnregisterFeature( const FeatureIdentity& feature ) override
		{
			// Wipe out feature address so feature pointers can know it's gone
			Repository::Get().FindFeature( feature )->fn = nullptr;
		}

		void FCI_CALLCONV RegisterFeaturePointer( IFeaturePointer* featurePointer ) override
		{
			featurePointers.push_back( featurePointer );
		}

		void FCI_CALLCONV UnregisterFeaturePointer( IFeaturePointer* featurePointer ) override
		{
			featurePointers.erase( std::find_if( std::begin(featurePointers), std::end(featurePointers), [featurePointer](const IFeaturePointer* entry)
			{
				return entry == featurePointer;
			} ), std::end(featurePointers) );
		}

		void FCI_CALLCONV LaunchSniffer() override
		{
			static bool sniffed = false;
			if( !sniffed )
			{
				sniffed = true;

				Sniffer::Sniff().RegisterFindings();
			}
		}

		// WIP
		/*void FCI_CALLCONV ValidateAllFeaturePointers() override
		{
			bool hasUnknownFeatures = false;
			std::vector<std::string> unknownFeatures;

			for( auto featurePointer : featurePointers )
			{
				auto state = featurePointer->GetModuleState();
				if( !Repository::Get().FindFeature( state.featureName ) )
				{
					featurePointer->Populate();

					if( !Repository::Get().FindFeature( state.featureName ) )
					{
						// This can happen with an expected DLL is missing or whose location was not added in FCI_SEARCH_PATHS
						hasUnknownFeatures = true;

						if( std::find( std::begin(unknownFeatures), std::end(unknownFeatures), state.featureName ) == unknownFeatures.end() )
						{
							unknownFeatures.push_back( state.featureName );
						}
					}
				}
			}

			if( hasUnknownFeatures )
			{
				std::string str( "Missing DLLs detected\n\nUnknown Features:\n" );
				
				for( const auto& unknownFeature : unknownFeatures )
				{
					str += unknownFeature + "\n";
				}

				throw str;
			}
		}*/

		FeatureQuery FCI_CALLCONV QueryFeature( const FeatureIdentity& feature ) override
		{
			CFeature* featureQuery = Repository::Get().FindFeature( feature );
			
			if( featureQuery )
			{
				return { {feature.featureName, featureQuery->id.moduleName.c_str(), featureQuery->id.priority},
					featureQuery->fn ? &featureQuery->fn : nullptr,
					featureQuery->fn ? eFeatureQueryStatus::FEATURE_READY : eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE };
			}

			return { feature, nullptr, eFeatureQueryStatus::UNKNOWN_FEATURE };
		}

		bool FCI_CALLCONV EnsureModuleLoaded( FeatureQuery feature ) override
		{
			if( feature.status == eFeatureQueryStatus::KNOWN_FEATURE_BUT_UNAVAILABLE )
			{
				if( FcIsModuleLoaded( feature.id.moduleName ) )
				{
					const auto err = std::string( "Feature '") + feature.id.featureName + "' in module '" +
						feature.id.moduleName + "' flagged as Unavailable but module is loaded.";
					throw FcInterfaceUnxpectedBehaviorException( err );
				}

				// Attempt to load the specified module this feature should reside in.
				HMODULE mod = LoadLibraryA( feature.id.moduleName );

				if( shouldSetUpDllSearchPaths && !mod )
				{
					shouldSetUpDllSearchPaths = false;

					// Try our special search paths once we invoke our first feature with an non-default location
					FCIModuleSearchPaths::SetupDLLSearchPaths();

					mod = LoadLibraryA( feature.id.moduleName );

					if( !mod )
					{
						return false;
					}
				}

				feature = FeaturePointerPopulator( &feature.fp, feature.id );

				if( !feature && mod != 0 )
				{
					// Unload this library since it was not previously loaded and we were only loading it to access said Feature
					FreeLibrary( mod );

					const auto err = std::string( "Failed to populate Feature '") + feature.id.featureName + "' after loading module '" +
						feature.id.moduleName + "'. Unloading module.";
					throw FcInterfaceUnxpectedBehaviorException( err );
				}
			}

			return feature;
		}

		private:

			std::vector<IFeaturePointer*>	featurePointers;
			bool	shouldSetUpDllSearchPaths = true;
	};
}

// Used by modules for registering their features at global construction
IFcInterface* FCI_EXPORT_CALLCONV FcInterfaceInternals::EL_REPOFETCHER_EXPORT_NAME()
{
	// Allocate this on the heap so it never destructs as we'll be accessing it during global destruction.
	static CFcInterface* gFcInterface = new CFcInterface;
	return gFcInterface;
}

#include "internal/modulesearchpath.cpp"
#include "internal/sniffer.cpp"
#include "internal/repository.cpp"