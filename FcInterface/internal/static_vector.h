#pragma once

#include <list>
#include <vector>

// static contiguous memory container without size restrictions
template<typename TElement, size_t ChunkSize>
struct static_vector final
{
	static_vector()
	{
		AddStaticBuffer();
	}

	void push_back( const TElement& elm )
	{
		if( memBench.back().size() == ChunkSize )
		{
			AddStaticBuffer();
		}

		memBench.back().push_back( elm );
	}

	template<typename Func>
	TElement* find( Func pred )
	{
		for( auto& bench : memBench )
		{
			for( auto& elm : bench )
			{
				if( pred(elm) )
				{
					return &elm;
				}
			}
		}

		return nullptr;
	}

private:

	void AddStaticBuffer()
	{
		memBench.emplace_back();
		memBench.back().reserve( ChunkSize );
	}

	std::list<std::vector<TElement>> memBench;
};