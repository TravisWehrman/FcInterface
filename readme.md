# FcInterface

FcInterface is a lightweight PIMPL framework loosely inspired by Valve Corporation's Source *CreateInterface* framework.
Used for registering *Features* (Concrete high-level components) within an application
and have them accessible throughout the application via *Feature Pointers* (PIMPL Interface).
FcInterface minimizes dependency management complexity making dependencies very fluid & easy to change.

Note: Only supports Windows at this time but can be easily upgraded to support multiple platforms.

# Framework Features
## Design Trade-offs
 * Favor *ease of use* and *minimal boilerplate* over minimizing runtime overhead

### No Link-time dependency imposed on Consumers
The consumer of an interface does not need to know where an interface resides.
Only that it exists and will be available at runtime.
The consuming library will not gain any additional link-time dependencies.

### No Compile-time dependency imposed by Features
Like in any well designed system, if its interface doesn't change then the underlying
implementation can be changed drastically (or swapped out at runtime) without requiring
the consumer to recompile or relink.

By imposing the dependency inversion principle, FcInterface promotes strong decoupling
between consumers and implementation details of a system.
Further promotes treating interfaces as a single source of truth (SSoT) for a given system
and helpers logically separate sub-domains.

### Automatic dependency management
FcInterface handles dependency management between application modules.
FcInterface will automatically load a module which supports a specific Feature when invoked
if the module is not currently loaded.

FcInterface does not automatically unload modules it loads, however if they are unloaded elsewhere,
FcInterface will handle it gracefully and load the module again if a Feature in the module 
is invoked after unload.

### Fluid Feature Management
Features are very fluid, meaning they can be easily moved to other modules without
requiring a recompile for any consumers for said Feature.
This also allows FcInterface to work with an application with thousands of modules,
or just a single executable. 
Again, Consumers don't care where the Feature lives, only that it'll be available at runtime.

### Minimal Framework "lock-in"
FcInterface only requires two lines of code to use, one for *Declaring* a Feature
("This is my interface for Feature x")
and one for registering the concrete ("This is my concrete for Feature x").
These two lines of code are additive and independent; they do not tangle themselves in with
your code.

Moving away from FcInterface means all you need to do is do a find-replace to delete the two lines
and to manually manage your dependencies since FcInterface handled this for you.

### Minimize mistakes when using Framework
FcInterface strives to be as simple as possible to use, but no simpler.
See "Troubleshooting" for the few errors you could run into.

### Cross-Compiler support
Because the only boundary between the interface and the concrete is an opaque pointer,
Features can be trivially invoked across some different C++ compilers.
However, your interfaces must be designed to be platform-agnostic.
Such as no STL (if differing impls), no cross-memory management, identical data alignment, etc.
But as long as each compiler can implement the vtable the same way (Which many do, by chance)
then you can.

Of course this is not necessarily recommended, it has it's real world use cases.

FcInterface has been tested with *Visual C++* talking to *Borland/Embarcadero BCC32C (Clang)* modules
and vice versa and is used in production applications.

### Possible Future Upgrades
The following features can be added to FcInterface without much effort
if good use-cases present themselves.

* Generic Plugins
  * Feature Pointers that can dynamically change concretes of same type.
* Mocking
  * Swap out Features for Mocked versions for testing purposes
* Versioning
  * Each Feature can have a corresponding version; allow multi-version support.
  * This requires developers to have strict rules around versioned interface modification.
  * Recommend to avoid if dev team has total control over code (i.e. no customer plugins; no public APIs)
  * Can be partially achieved by using a different Feature name and use the newer Feature where desired.
* FeatureSniffer: Application filtering
  *  May be worthy to add a name filter in case multiple applications exists in the same directories.
* Automatic concrete registration? (Not sure if possible yet)

# Technical
## How it works
FcInterface stores a process-wide repository of all registered *Features* in an application's .exe.
For each *Feature*, the repository stores the pointer to the feature's implementation (Concrete Instance),
the associated name of the feature and the name of the module it came from.

Any module (DLL or EXE) can declare *Features*. Said *Features* are automatically registered when the module is loaded.
*Feature Pointers* are invoked throughout the application to use said *Feature*.

If a *Feature Pointer* is invoked for a *Feature* whose module containing it is not loaded,
the module will be automatically loaded provided it exists at runtime, allowing its execution.

### Feature Pointers
A **Feature Pointer** is a lightweight object that is used like a pointer to invoke a registered feature's methods.

Example:

`MyFeature->DoSomething()`

Feature Pointers use virtual dispatching to invoke function calls at runtime.

### Feature
A **Feature** is a concrete instance of a class that exposes an abstract interface to consumers.

*Feature Pointers* reference a single feature.

### Interface
The **Interface** is just the binding contract between a Feature and a Feature Pointer, a pure virtual class.

#### Recommended Design
Interfaces typically should be high-level facades representing a system aimed towards ease of use; hiding as much detail as possible.
Interfaces should also try to only accept primitives or foundational domain data structures as arguments.
Otherwise they should provide their own data structures in the interface header file.

Interfaces should not contain any data members or non-virtual functions.
Some compile-time checks are in place to help enforce this (C++11 required).

#### Interface Inheritance
Interfaces can inherit from other Interfaces without issue outside of versioning.
With versioning, if you change a base class, this changes the vtable layout of inherited classes as well.
Since we are not supporting versioning at this time as we rebuild everything this is a non-issue.

### FeatureSniffer (Autoloading)
**FeatureSniffer** is an async thread that scans all application binaries (See `FCI_SNIFFER_PATHS`)
 for features on application launch.
Used for the Autoloading feature.

This is done so we can preemptively register all features so when we invoke a *Feature Pointer* whose *Feature*'s
module is not loaded, we can automatically load the corresponding module when needed.

NOTE: This does not load the binaries as calling `LoadLibrary()` before `main()` terminates the program.
Instead it loads the binaries as binary images and extracts feature names from dummy exports.
This way we know what features exist in what modules.

Modules are never automatically unloaded by FcInterface, but if they are FcInterface will handle it gracefully.

## Performance
#### Registering Features
O(n<sup>2</sup>) [contiguous]

One Second Overhead Mark (optimized build): ~35,000 *Features*

Can knock this down to O(log n) with a lot of extra work but 35K Features is likely more
Features than even the largest of applications would have.
If all 35K features register at once it takes about one second.
Going to leave this O(n<sup>2</sup>) [contiguous] as it is well within target requirements.

#### Registering Feature Pointers
O(n)
 * Requires *Features* to live in static memory.
 * No *Feature Pointer* registration required.


O(n<sup>2</sup>)

If *Features* must be movable, then *Feature Pointers* need to be registered
to be tracked and updated when *Features* unregister.

#### Invoking Feature Pointers
Overhead vs single virtual dispatch (initial invocation; no module load): ~25%
 * Will be "slow" if we have to load modules

Overhead vs single virtual dispatch (subsequent): ~3%

See FcInterfaceTests to run benchmarks.

## Usage: Create, Register, and use an Interface

#### Step 0: Project Settings

##### Define `FCI_IS_REPO_MODULE` in a project
The sole module that contains FcInterface.cpp (The Feature Repository) must define FCI_IS_REPO_MODULE.
This is usually in the .exe project for an application.

If the module is not an exe, define `FCI_LOCATION_NAME` so other modules can know where to query for Features.

Format: `FCI_LOCATION_NAME=dllname`
(no quotes)

##### Define `FCI_SNIFFER_PATHS` (Optional)
FcInterface will scan these paths relative to the root of the `FCI_IS_REPO_MODULE` module.
FcInterface will scan each module in these paths (except for .exes) for Features.
This is so FcInterface can know what module to automatically load when a *Feature Pointer* is invoked for a *Feature* that resides in an unloaded module.

NOTE: This scanning process occurs in a separate thread and is dispatched at the start of the program.

FcInterface will automatically scan all modules in the same directory as the `FCI_IS_REPO_MODULE` module.

Format `FCI_SNIFFER_PATHS="Folder1,Folder2,Folder3/subFolder"`
(quotes required)

#### Step 1: Create an Interface
Create an abstract facade contract/interface for your feature. Declare it with FCINTERFACE_DECLARE()
```
// IMyFeature.h

FCINTERFACE_DECLARE( IInterfaceClassName, FeaturePointerName
{
	virtual const char* GetFeatureName() = 0;
})

// Optional
// This is recommended over FCINTERFACE_DECLARE since some IDE's don't display code within macros well (syntax highlighting, Intellisense, etc.)
FCINTERFACE_DECLARE_EXISTING( IInterfaceClassName, FeaturePointerName )
```
Above, we've declared an abstract interface for a feature that contains a single pure virtual method.

Any additional Feature Pointers using the same interface but a different concrete can use `FCINTERFACE_DECLARE_EXISTING()`

#### Step 2: Concrete Implementation and Registration

Now let's implement it with a facade class:
```
// CMyFeature.cpp

struct CMyFeature final : public IInterfaceClassName
{
    const char* GetFeatureName() override
    {
	    return "I am MyFeature!\n";
    }
};

FCINTERFACE_REGISTER( CMyFeature, IMyFeature, FeaturePointerName )
```

Now that we have our feature implemented, we can register the interface with the helper macro at the end.

Your new feature called "FeaturePointerName" and is now registered and available for consumption throughout the application.

#### Step 3: Use it
```
#include "public/IMyFeature.h

void main()
{
    // 'FeaturePointerName' is ready for immediate use.
    std::cout << FeaturePointerName->GetFeatureName();
}
```

Invoking a *Feature Pointer* whose *Feature* does not exist will throw an exception.
This either means the module the *Feature* is in was not found (See FCI_SNIFFER_PATHS), or the user forgot to FCINTERFACE_REGISTER the concrete.

The output of this main() is:
```
I am MyFeature from FcFeature.dll!
```

We've successfully created and used a feature that could be used and implemented in any module (or the same), without requiring any link-time dependencies between the two modules.

## Feature Groups
If desired, you can group similar Feature Pointers together in a domain API like such:
```
// TODO: Helper macro not really possible?

static struct CMyFeatures final
{
	CMyFeatures* operator->() { return this; }
	FeatureInterface1* Feature1() { return *::Feature; }
	FeatureInterface2* Feature2() { return *::Feature2; }
} MyFeatures;
```

This helps organize Features in a larger superdomain to further restrict scope.

NOTE: It's recommended to not create Feature Pointers for Features that are also used in a group.
Would be awkward to call the same feature both ways, should be mutually exclusive.

```
void foo()
{
	MySuperDomain->SubDomain1()->DoStuff();
	MySuperDomain->SubDomain2()->DoStuff();
}
```

## Relationships
In most use-cases, Interfaces and Features have a one-to-one relationship. That is, a `FCINTERFACE_DECLARE()`
will have a corresponding `FCINTERFACE_REGISTER()`.
However you sometimes may want a one-to-many relationship between Interfaces and Features respectively.

To achieve such you can declare additional Features that use the same Interface via the
`FCINTERFACE_DECLARE_EXISTING()` helper macro in the interface header file.
This creates a new Feature Pointer for the same Interface, that should register a different concrete.

## Plugins (WIP - Not Yet Supported)

Furthermore, you may not know how many Features or Interfaces will be present at compile-time.
You may need something more generic to enable a plugin-like system.

If a Feature is not known to the consumer (core app), than it is a plugin. If the consumer (core app) must know about a Feature at compile time, then it is not a plugin.

By default, Feature Pointers are a source-time construct. Which means when you use a Feature, you explicitly invoke
the Feature Pointer in code.
This is ideal for most scenarios but not all.
For example, if you want to support a generic plugin system, static Feature Pointers don't cut it since
you must know ahead of time all of the plugins you will support (not generic), and must explicitly invoke each respective Feature Pointer to use it (not dynamic).

This is where dynamic Feature Pointers come in. DFPs enable using Features with generic plugin-like systems.

**Plugin**: A Feature (typically located in an external module) based on a shared Interface that does not provide a Feature Pointer.

Plugins of the same "type" all share the same interface but they do not declare their own Feature Pointer since that is a compile-time construct.

`FCINTERFACE_DECLARE_PLUGIN()`

`CDynamicFeaturePointer<InterfaceType>` allows you to reuse the same FP of a specific interface for different concretes.

Since Plugins are both generic and dynamic, they must not have more than one Feature using the same "plugin" interface.
This way the main Application does not need to know anything about the Plugin at compile-time.
It can just load it and use a DFP to use it.

For example, you may want to swap Features in a Feature Pointer (that use the same interface) at run-time.

However, sometimes you may want to create a Feature Pointer that uses the same interface, but can change Features at runtime.
This is particularly useful for Plugins, where you just want to load a a series of Features and execute the same code on them,

// TODO: can we just "Get" the FP (plugin interface) from a plugin when it loads?
// Like, GetPluginPointer( HMODULE ) ?

```
void foo()
{
   CDynamicFeaturePointer<InterfaceType> PluginPtr( "ModuleName?" );
   PluginPtr->DoPluginStuff();
   FeaturePtr.Repopulate( "FeatureName2" );

}
```

## Troubleshooting
### My FeaturePointer threw an exception!
This means the *Feature* associated with this *Feature Pointer* is not registered.
This is usually caused by one of the following reasons:
1. Someone forgot to register the Feature via `FCINTERFACE_REGISTER()`
1. The Feature name is mismatched in `FCINTERFACE_DECLARE()` and `FCINTERFACE_REGISTER()`
1. The module the Feature resides in was not found (See `FCI_SNIFFER_PATHS`)

### Invoking a Feature Pointer results in an Access Violation
This is likely caused by module the Feature resides in has not been recompiled for any Interface changes or was built with different compiler settings.

## Helper Macros
### `FCINTERFACE_DECLARE()`
Declare an abstract class that is intended to be used with FcInterface.

Creates a Feature Pointer instance.

#### Arguments
###### `interfaceClassName`
interface type

###### `globalFeatureName`
Feature Pointer Identifier, used to access to said Feature Pointer in interface header

###### `classBody`
Body of your interface. This is wrapper inside the macro.

*Do not implement any functions here as you will not be able to debug into them since this is a part of a macro*

### `FCINTERFACE_DECLARE_EXISTING()`
Declare an additional Feature Pointer class that uses the same interface, but a different concrete.

Recommended over FCINTERFACE_DECLARE() to avoid IDE syntax highlighting, Intellisense, etc. discrepancies

### `FCINTERFACE_REGISTER()`
Registers feature to be queryable throughout the application.

#### Arguments

###### `concreteClass`
class type used to create the concrete

###### `abstractClass`
class type interface used when returning queries

Purely used to create unique names in case multiple concretes register to the same Feature.
// TODO This may not be needed anymore

###### `interfaceName`
Future Identifier
Use the same `globalFeatureName` argument used in `FCINTERFACE_DECLARE()` 