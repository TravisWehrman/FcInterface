// The sole purpose of this is to repopulate the removed search paths that SetDefaultDllDirectories() destroys
// SetDefaultDllDirectories() is needed to add more than one path to the search path via AddDllDirectory() for auto-loaded modules in Windows.

// The main search path that is missing is the "PATH" environment variable

// Default Windows Search order:
// https://docs.microsoft.com/en-us/windows/win32/dlls/dynamic-link-library-search-order
// 1. The directory from which the application loaded.
// 2. The system directory. Use the GetSystemDirectory function to get the path of this directory.
// 3. The 16-bit system directory. There is no function that obtains the path of this directory, but it is searched.
// 4. The Windows directory. Use the GetWindowsDirectory function to get the path of this directory.
// 5. The current directory.
// 6. The directories that are listed in the PATH environment variable. Note that this does not include the per-application path specified by the App Paths registry key. The App Paths key is not used when computing the DLL search path.

// Search order After SetDefaultDllDirectories():
// https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-setdefaultdlldirectories
// 1. The directory that contains the DLL (LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR). This directory is searched only for dependencies of the DLL being loaded.
// 2. The application directory (LOAD_LIBRARY_SEARCH_APPLICATION_DIR).
// 3. Paths explicitly added to the application search path with the AddDllDirectory function (LOAD_LIBRARY_SEARCH_USER_DIRS) or the SetDllDirectory function. If more than one path has been added, the order in which the paths are searched is unspecified.
// 4. The System directory (LOAD_LIBRARY_SEARCH_SYSTEM32).

#include "modulesearchpath.h"
#include "platform.h"
#include "platform_internal.h"
#include "sniffer.h"

#include <sstream>

namespace
{
	void FCIAddDllDirectory( const fs::path& fspath )
	{
		const std::string path = fs::path( fspath ).make_preferred().string();

		FcAddDllDirectory( path );
	}

	void AddSnifferDirectoriesToDLLSearchPath()
	{
		for( const auto& dir : Sniffer::GetSnifferDirs().dirs )
		{
			FCIAddDllDirectory( dir );
		}
	}

	void AddRuntimeDependenciesToDLLSearchPath()
	{
#if defined(FCI_DEPENDENCY_PATHS)
		std::string runtimeDepPaths( FCI_TO_STRING( FCI_DEPENDENCY_PATHS ) );
		runtimeDepPaths.erase( std::remove( runtimeDepPaths.begin(), runtimeDepPaths.end(), '\"'), runtimeDepPaths.end() );

		std::string dir;
		std::istringstream ss( runtimeDepPaths );
		while( std::getline( ss, dir, ',' ) )
		{
			FCIAddDllDirectory( dir );
		}
#endif
	}

	void PopulatePATHPaths()
	{
#if defined(_MSC_VER)
		char* env = nullptr;
		size_t sz;

		if( _dupenv_s( &env, &sz, "PATH") == 0 && env )
#elif defined(__BORLANDC__)
		const char* env = std::getenv( "PATH" );

		if( env )
#endif
		{
			std::string split;
			std::istringstream ss( env );
			while( std::getline( ss, split, ';' ) )
			{
				FCIAddDllDirectory( split );
			}

#if defined(_MSC_VER)
			free( env );
#endif
		}

	}

	void AddMissingDefaultSearchPaths()
	{
		PopulatePATHPaths();
	}
}

namespace FCIModuleSearchPaths
{
	void SetupDLLSearchPaths()
	{
		// Used so when delay-loaded modules are loaded by dependencies, they can be found
		// by including dirs added in AddDllDirectory() via LOAD_LIBRARY_SEARCH_USER_DIRS

		// NOTE: It appears Windows does not allow an easy way to add multiple DLL search paths for Auto-loading modules.
		// In order to add multiple paths, we must use AddDllDirectory(), however it is not used by the auto-loader without SetDefaultDllDirectories()
		// However, This permanently removes many search paths like the "PATH" env so we'll have to repopulate those for the desired behavior
		// https://docs.microsoft.com/en-us/windows/win32/api/libloaderapi/nf-libloaderapi-setdefaultdlldirectories
		SetDefaultDllDirectories( LOAD_LIBRARY_SEARCH_DEFAULT_DIRS );

		AddMissingDefaultSearchPaths();

		AddRuntimeDependenciesToDLLSearchPath();
		AddSnifferDirectoriesToDLLSearchPath();
	}
}