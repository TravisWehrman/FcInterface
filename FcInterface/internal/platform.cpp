#pragma once

#include "filesystem.h"

#include <string>

#if defined(_WIN32)
#include <Windows.h>

bool FcIsModuleLoaded( const char* moduleName )
{
	return GetModuleHandleA( moduleName ) != nullptr;
}

void* FcGetProcAddress( const char* moduleName, const char* procName )
{
	return GetProcAddress( GetModuleHandleA( moduleName ), procName );
}

void FcLoadLibraryIfNotLoaded( const char* moduleName )
{
	if( !FcIsModuleLoaded( moduleName ) )
	{
		LoadLibraryA( moduleName );
	}
}

std::string FcGetThisModuleName( const char* (*funcInModule)() )
{
	char buf[MAX_PATH];
	HMODULE mod = nullptr;

	GetModuleHandleExA( GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, (LPCSTR)funcInModule, &mod );
	GetModuleFileNameA( mod, buf, sizeof( buf ) );

	fs::path dllPath = buf;
	std::string dllName = dllPath.filename().string();
	std::transform( dllName.begin(), dllName.end(), dllName.begin(), ::tolower );

	return dllName;
}
#else
#error "FcInterface TODO: Map other platform OS calls"
#endif