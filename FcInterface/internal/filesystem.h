#pragma once

#include <vector>

#if FCI_CPP17_SUPPORT
#include <filesystem>
namespace fs = std::filesystem;
#else
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;
#endif

using PathList = std::vector<fs::path>;